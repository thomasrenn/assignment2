#Assignment 2

## Particle effect
 - Added a steam particle effect to all the ghosts and it is always on, following the ghosts.

## Linear Interpolation
 - Added an interpolation effect to the WaypointPatrol Script so that the volume of the 
SFXGhostMove Audio source for all of the ghosts. The closer the player gets to the ghost the
louder the audio is and the volume drops of faster than without the interpolation. 

## Dot-Product
 - Added a lighting effect to the first gargoyle in the main starting room. The intensity of the light intensity increases
   when the JohnLemon character is in front of the gargoyle, within a speciefied view. When John is not within the
   view the light intensity decreases. 


## Names
Thomas Renn completed all requirements.