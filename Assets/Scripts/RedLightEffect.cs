using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedLightEffect : MonoBehaviour
{
    public Transform player;
    private Quaternion original;
    Light lightComponent;

    // Start is called before the first frame update
    void Start()
    {
        lightComponent = GetComponent<Light>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
        // Normalize the Vectors
        Vector3 toTarget = (player.position - transform.position).normalized;


        // Take the dot product of the gargoyle vector and the player vector
        if (Vector3.Dot(toTarget, transform.forward) > 0.8)
        {
            Debug.Log("IN FRONT");
            lightComponent.intensity += 2;
        }
        else
        {
            if (lightComponent.intensity > 20)
            {
                lightComponent.intensity -= 5;
            }
        }
    }
}
