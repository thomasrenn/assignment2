using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Animator m_Anmimator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSorce;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity; // Used for stroring rotations.

    public float turnSpeed = 20f;

    // Start is called before the first frame update
    void Start()
    {
        m_Anmimator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSorce = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        
        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        // Check for rotation.
        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);

        // Update isWalking
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Anmimator.SetBool("isWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSorce.isPlaying)
            {
                m_AudioSorce.Play();
            }
        }
        else
        {
            m_AudioSorce.Stop();
        }
        
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }


    private void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Anmimator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }
}
